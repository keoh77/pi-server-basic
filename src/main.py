import os
import uuid

import aiohttp_jinja2
import jinja2
from aiohttp import web
import motor.motor_asyncio

import settings
from views import setup_views

app = web.Application()

aiohttp_jinja2.setup(
    app, loader=jinja2.FileSystemLoader(settings.TEMPLATES_ROOT))

setup_views(app)
app.router.add_static('/static/',
                      path=settings.STATIC_ROOT,
                      name='static')

motor_client = motor.motor_asyncio.AsyncIOMotorClient(settings.DATABASE['URI'])
app['db'] = motor_client[settings.DATABASE['DB_NAME']]

app['app_instance'] = {
    'id': str(uuid.uuid4()),
    'process': os.environ.get('INSTANCE', 1),
    'version': os.environ.get('VERSION', '-')
}

web.run_app(app, port=8080)
