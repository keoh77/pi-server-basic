"""Base library tests."""
from src.utils import get_context


class FakeRequest:

    app = {
        'app_instance': 'instance'
    }


async def test_utils():
    """Context util tests."""

    fake_request = FakeRequest()
    context = await get_context(fake_request)

    assert context == {'app_instance': 'instance'}
