
from aiohttp_jinja2 import render_template
from utils import get_context


async def index(request):
    context = await get_context(request)
    context['text'] = await request.app['db']['test'].find_one(
        {'title': 'test'})
    print("Vista index ejecutada")
    return render_template('index.html', request, context)
