
async def get_context(request):
    context = {
        'app_instance': request.app['app_instance'],
    }
    return context
