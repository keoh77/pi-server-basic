import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
TEMPLATES_ROOT = os.path.join(BASE_DIR, 'templates')

DATABASE = {
    'URI': os.environ.get('MONGO_URI', 'mongodb://localhost:27017'),
    'DB_NAME': 'basic'
}
